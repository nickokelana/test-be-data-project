package id.co.cimbniaga.octomobile.project.domain.dao;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ProjectAssignment {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long idass;

    @ManyToOne
    @JoinColumn(name="idproject", referencedColumnName="idproject")
    private Project project;

    @ManyToOne
    @JoinColumn(name="idleader", referencedColumnName="idemployee")
    private Employee leader;

    private Date dtCreated;

    private Date dtModified;

    @ManyToMany
    @JoinTable(
        name = "project_assignment_member",
        joinColumns = {@JoinColumn(name = "idass", referencedColumnName = "idass")},
        inverseJoinColumns = {@JoinColumn(name = "idemployee", referencedColumnName = "idemployee")})
    private Set<Employee> members = new HashSet<>();

    public ProjectAssignment() {
    }

    public Long getIdass() {
        return idass;
    }

    public void setIdass(Long idass) {
        this.idass = idass;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Employee getLeader() {
        return leader;
    }

    public void setLeader(Employee leader) {
        this.leader = leader;
    }

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    public Date getDtModified() {
        return dtModified;
    }

    public void setDtModified(Date dtModified) {
        this.dtModified = dtModified;
    }

    public Set<Employee> getMembers() {
        return members;
    }

    public void setMembers(Set<Employee> members) {
        this.members = members;
    }
}
