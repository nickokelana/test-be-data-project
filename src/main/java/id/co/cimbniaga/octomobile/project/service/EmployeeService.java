package id.co.cimbniaga.octomobile.project.service;

import id.co.cimbniaga.octomobile.project.component.AbstractEntityService;
import id.co.cimbniaga.octomobile.project.domain.dao.Employee;
import id.co.cimbniaga.octomobile.project.domain.dto.common.EmployeeDTO;
import id.co.cimbniaga.octomobile.project.domain.mapper.EmployeeMapperImpl;
import id.co.cimbniaga.octomobile.project.repository.EmployeeRepository;
import id.co.cimbniaga.octomobile.project.util.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService extends AbstractEntityService<Employee, EmployeeDTO, Long> {

    @Autowired
    ValidationUtils validationUtils;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.entityRepository = employeeRepository;
        this.entityMapper = new EmployeeMapperImpl();
    }

    public EmployeeRepository repository() {
        return (EmployeeRepository) entityRepository;
    }

    public void initData() {
        entityRepository.save(new Employee("EM001", "AGUS", "12345", true));
        entityRepository.save(new Employee("EM002", "BUDI", "67890", true));
        entityRepository.save(new Employee("EM003", "GINANJAR", "11111", true));
    }

    public EmployeeDTO create(EmployeeDTO employeeDTO) throws Exception {
        // validation
        List<Employee> exist = repository().findByEmployeeNumber(employeeDTO.getEmployeeNumber());
        if (exist.size() > 0) {
            throw new Exception("Employee Number must be unique!");
        }

        EmployeeDTO result = save(employeeDTO);

        return result;
    }

    public EmployeeDTO update(EmployeeDTO employeeDTO) throws Exception {
        // validation
        List<Employee> exist = repository().findByEmployeeNumber(employeeDTO.getEmployeeNumber());
        if(exist.stream().filter(x -> x.getIdemployee() != employeeDTO.getId() && x.getEmployeenumber().contains(employeeDTO.getEmployeeNumber())).findAny().orElse(null) != null) {
            throw new Exception("Employee Number must be unique!");
        }

        EmployeeDTO result = save(employeeDTO);

        return result;
    }

    @Override
    public void validation(EmployeeDTO dto) throws Exception {
        if (validationUtils.isNullOrEmpty(dto.getEmployeeNumber())) {
            throw new Exception("field employeeNumber is null or empty");
        }
        if (validationUtils.isNullOrEmpty(dto.getName())) {
            throw new Exception("field code is null or empty");
        }
        if (validationUtils.isNullOrEmpty(dto.getPersonalIdNumber())) {
            throw new Exception("field personalIdNumber is null or empty");
        }
    }
}
