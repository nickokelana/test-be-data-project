package id.co.cimbniaga.octomobile.project.controller;

import id.co.cimbniaga.octomobile.project.domain.dao.Employee;
import id.co.cimbniaga.octomobile.project.domain.dto.common.EmployeeDTO;
import id.co.cimbniaga.octomobile.project.service.EmployeeService;
import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/v1/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @PostMapping("/init")
    public ResponseEntity<String> initData() {
        employeeService.initData();
        return ResponseEntity.ok("OK");
    }

    @PostMapping("")
    public ResponseEntity<List<EmployeeDTO>> getEmployee(Pageable pageable) {
        Page<EmployeeDTO> page = employeeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PostMapping("/detail/{id}")
    public ResponseEntity<EmployeeDTO> getEmployeeById(@PathVariable Long id) {
        EmployeeDTO result = employeeService.findOne(id);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/create")
    public ResponseEntity<EmployeeDTO> createEmployee(@RequestBody EmployeeDTO dto) {
        try
        {
            if (dto.getId() != null) {
                throw new Exception("ID NOT NULL!");
            }
            EmployeeDTO data = employeeService.create(dto);
            return ResponseEntity.ok().body(data);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.badRequest().header("X-Message", e.getMessage()).body(dto);
        }
    }

    @PostMapping("/update")
    public ResponseEntity<EmployeeDTO> updateEmployee(@RequestBody EmployeeDTO dto) {
        try
        {
            if (dto.getId() == null) {
                throw new Exception("ID NULL!");
            }
            EmployeeDTO data = employeeService.update(dto);
            return ResponseEntity.ok().body(data);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.badRequest().header("X-Message", e.getMessage()).body(dto);
        }
    }

    @PostMapping("/delete")
    public ResponseEntity<EmployeeDTO> deleteEmployee(@RequestBody EmployeeDTO dto) {
        try
        {
            if (dto.getId() == null) {
                throw new Exception("ID NULL!");
            }
            employeeService.delete(dto.getId());
            return ResponseEntity.ok().body(dto);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.badRequest().header("X-Message", e.getMessage()).body(dto);
        }
    }
}
