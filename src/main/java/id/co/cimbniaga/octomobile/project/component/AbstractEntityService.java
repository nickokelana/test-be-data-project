package id.co.cimbniaga.octomobile.project.component;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public abstract class AbstractEntityService<E, D, ID> {

    protected JpaRepository<E, ID> entityRepository;

    protected EntityMapper<D, E> entityMapper;

    public Page<D> findAll(Pageable pageable) { return entityRepository.findAll(pageable).map(x -> entityMapper.toDto(x)); }

    public abstract void validation(D dto) throws Exception;

    public E find(ID id) {
        if (entityRepository.existsById(id)) {
            return entityRepository.getOne(id);
        } else {
            return null;
        }
    }

    public D findOne(ID id) {
        if (entityRepository.existsById(id)) {
            return entityMapper.toDto(entityRepository.getOne(id));
        } else {
            return null;
        }
    }

    @Transactional
    public D save(D dto) {
        E data = entityMapper.toEntity(dto);
        data = entityRepository.saveAndFlush(data);
        dto = entityMapper.toDto((data));
        return dto;
    }

    @Transactional
    public void delete(ID id) throws Exception {
        Optional<E> data = entityRepository.findById(id);
        if (data.isEmpty()) {
            throw new Exception("ID " + id + " NOT FOUND!");
        }
        entityRepository.delete(data.get());
    }
}