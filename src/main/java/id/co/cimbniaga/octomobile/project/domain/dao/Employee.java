package id.co.cimbniaga.octomobile.project.domain.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long idemployee;

    private String employeenumber;

    private String name;

    private String personalidnumber;

    private Boolean active;

    public Employee() {
    }

    public Employee(String employeenumber, String name, String personalidnumber, Boolean active) {
        this.employeenumber = employeenumber;
        this.name = name;
        this.personalidnumber = personalidnumber;
        this.active = active;
    }

    public Long getIdemployee() {
        return idemployee;
    }

    public void setIdemployee(Long idemployee) {
        this.idemployee = idemployee;
    }

    public String getEmployeenumber() {
        return employeenumber;
    }

    public void setEmployeenumber(String employeenumber) {
        this.employeenumber = employeenumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersonalidnumber() {
        return personalidnumber;
    }

    public void setPersonalidnumber(String personalidnumber) {
        this.personalidnumber = personalidnumber;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "idemployee=" + idemployee +
                ", employeenumber='" + employeenumber + '\'' +
                ", name='" + name + '\'' +
                ", personalidnumber='" + personalidnumber + '\'' +
                ", active=" + active +
                '}';
    }
}
