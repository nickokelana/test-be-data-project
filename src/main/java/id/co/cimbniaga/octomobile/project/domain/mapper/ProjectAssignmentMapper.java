package id.co.cimbniaga.octomobile.project.domain.mapper;

import id.co.cimbniaga.octomobile.project.component.EntityMapper;
import id.co.cimbniaga.octomobile.project.domain.dao.Employee;
import id.co.cimbniaga.octomobile.project.domain.dao.Project;
import id.co.cimbniaga.octomobile.project.domain.dao.ProjectAssignment;
import id.co.cimbniaga.octomobile.project.domain.dto.common.EmployeeDTO;
import id.co.cimbniaga.octomobile.project.domain.dto.common.ProjectAssignmentDTO;
import id.co.cimbniaga.octomobile.project.domain.dto.common.ProjectDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.*;
import java.util.stream.Collectors;

@Mapper(uses = {
        ProjectMapper.class,
        EmployeeMapper.class
})
public interface ProjectAssignmentMapper extends EntityMapper<ProjectAssignmentDTO, ProjectAssignment> {

    ProjectMapper projectMapper = Mappers.getMapper( ProjectMapper.class );
    EmployeeMapper employeeMapper = Mappers.getMapper( EmployeeMapper.class );

    default ProjectAssignment toEntity(ProjectAssignmentDTO dto) {
        ProjectAssignment entity = new ProjectAssignment();
        entity.setIdass(dto.getProjectAssignmentId());
        entity.setProject(projectMapper.toEntity(dto.getProjectDTO()));
        entity.setLeader(employeeMapper.toEntity(dto.getLeader()));
        entity.setDtCreated(dto.getDateCreated());
        entity.setDtModified(dto.getDateModified());
        Set<Employee> members = new HashSet<>();
        for (EmployeeDTO employee : dto.getMembers()) {
            members.add(employeeMapper.toEntity(employee));
        }
        entity.setMembers(members);
        return entity;
    }

    default ProjectAssignmentDTO toDto(ProjectAssignment entity) {
        ProjectAssignmentDTO dto = new ProjectAssignmentDTO();
        dto.setId(entity.getIdass());
        dto.setProjectAssignmentId(entity.getIdass());
        dto.setProjectDTO(projectMapper.toDto(entity.getProject()));
        dto.setLeader(employeeMapper.toDto(entity.getLeader()));
        dto.setDateCreated(entity.getDtCreated());
        dto.setDateModified(entity.getDtModified());
        List<EmployeeDTO> members = new ArrayList<EmployeeDTO>();
        for (Employee employee : entity.getMembers()) {
            members.add(employeeMapper.toDto(employee));
        }
        dto.setMembers(members);
        return dto;
    }

    default ProjectAssignment fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProjectAssignment entity = new ProjectAssignment();
        entity.setIdass(id);
        return entity;
    }
}
