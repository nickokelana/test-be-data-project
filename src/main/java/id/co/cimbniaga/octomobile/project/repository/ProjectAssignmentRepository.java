package id.co.cimbniaga.octomobile.project.repository;
import id.co.cimbniaga.octomobile.project.domain.dao.ProjectAssignment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectAssignmentRepository extends JpaRepository<ProjectAssignment, Long> {
    @Query("select e from ProjectAssignment e")
    Page<ProjectAssignment> findAll(Pageable pageable);

    @Query("select e from ProjectAssignment e where e.project.idproject=:projectId")
    List<ProjectAssignment> findByProjectId(@Param("projectId") Long projectId);
}
