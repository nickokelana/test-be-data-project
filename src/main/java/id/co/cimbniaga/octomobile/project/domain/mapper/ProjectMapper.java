package id.co.cimbniaga.octomobile.project.domain.mapper;

import id.co.cimbniaga.octomobile.project.component.EntityMapper;
import id.co.cimbniaga.octomobile.project.domain.dao.Project;
import id.co.cimbniaga.octomobile.project.domain.dto.common.ProjectDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper()
public interface ProjectMapper extends EntityMapper<ProjectDTO, Project> {

    @Mapping(source = "idproject", target= "id")
    @Mapping(source = "idproject", target= "projectId")
    @Mapping(source = "code", target= "code")
    @Mapping(source = "name", target= "name")
    @Mapping(source = "description", target= "description")
    @Mapping(source = "datestart", target= "dateStart")
    @Mapping(source = "dateend", target= "dateEnd")
    ProjectDTO toDto(Project Project);

    @Mapping(source = "projectId", target= "idproject")
    @Mapping(source = "code", target= "code")
    @Mapping(source = "name", target= "name")
    @Mapping(source = "description", target= "description")
    @Mapping(source = "dateStart", target= "datestart")
    @Mapping(source = "dateEnd", target= "dateend")
    Project toEntity(ProjectDTO dto);

    default Project fromId(Long id) {
        if (id == null) {
            return null;
        }
        Project entity = new Project();
        entity.setIdproject(id);
        return entity;
    }
}
