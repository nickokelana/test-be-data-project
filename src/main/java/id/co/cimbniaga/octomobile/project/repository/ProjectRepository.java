package id.co.cimbniaga.octomobile.project.repository;
import id.co.cimbniaga.octomobile.project.domain.dao.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    @Query("select e from Project e")
    Page<Project> findAll(Pageable pageable);

    @Query("select e from Project e where e.code=:code")
    List<Project> findByCode(@Param("code") String code);
}
