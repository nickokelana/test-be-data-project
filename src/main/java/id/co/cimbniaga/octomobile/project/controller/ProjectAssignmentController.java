package id.co.cimbniaga.octomobile.project.controller;

import id.co.cimbniaga.octomobile.project.domain.dto.common.ProjectAssignmentDTO;
import id.co.cimbniaga.octomobile.project.domain.dto.common.ProjectDTO;
import id.co.cimbniaga.octomobile.project.domain.dto.common.RequestAssignProjectDTO;
import id.co.cimbniaga.octomobile.project.service.ProjectAssignmentService;
import id.co.cimbniaga.octomobile.project.service.ProjectService;
import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/v1/project-assignment")
public class ProjectAssignmentController {

    @Autowired
    ProjectAssignmentService projectAssignmentService;

    @PostMapping("/init")
    public ResponseEntity<String> initData() {
        try {
            projectAssignmentService.initData();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok("OK");
    }

    @PostMapping("")
    public ResponseEntity<List<ProjectAssignmentDTO>> getProjectAssignment(Pageable pageable) {
        Page<ProjectAssignmentDTO> page = projectAssignmentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PostMapping("/detail/{id}")
    public ResponseEntity<ProjectAssignmentDTO> getProjectAssignmentById(@PathVariable Long id) {
        ProjectAssignmentDTO result = projectAssignmentService.findOne(id);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/assign-member")
    public ResponseEntity<RequestAssignProjectDTO> assignmember(@RequestBody RequestAssignProjectDTO dto) {
        try
        {
            if (dto.getEmployeeId() == null) {
                throw new Exception("employeeId can't null!");
            }
            if (dto.getProjectId() == null) {
                throw new Exception("projectId can't null!");
            }
            projectAssignmentService.assignMember(dto);
            return ResponseEntity.ok().body(dto);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.badRequest().header("X-Message", e.getMessage()).body(dto);
        }
    }

    @PostMapping("/assign-leader")
    public ResponseEntity<RequestAssignProjectDTO> createLeader(@RequestBody RequestAssignProjectDTO dto) {
        try
        {
            if (dto.getEmployeeId() == null) {
                throw new Exception("employeeId can't null!");
            }
            if (dto.getProjectId() == null) {
                throw new Exception("projectId can't null!");
            }
            projectAssignmentService.assignLeader(dto);
            return ResponseEntity.ok().body(dto);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.badRequest().header("X-Message", e.getMessage()).body(dto);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<ProjectAssignmentDTO> createProjectAssignment(@RequestBody ProjectAssignmentDTO dto) {
        try
        {
            if (dto.getId() != null) {
                throw new Exception("ID NOT NULL!");
            }
            ProjectAssignmentDTO data = projectAssignmentService.create(dto);
            return ResponseEntity.ok().body(data);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.badRequest().header("X-Message", e.getMessage()).body(dto);
        }
    }

    @PostMapping("/update")
    public ResponseEntity<ProjectAssignmentDTO> updateProjectAssignment(@RequestBody ProjectAssignmentDTO dto) {
        try
        {
            if (dto.getId() == null) {
                throw new Exception("ID NULL!");
            }
            ProjectAssignmentDTO data = projectAssignmentService.update(dto);
            return ResponseEntity.ok().body(data);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.badRequest().header("X-Message", e.getMessage()).body(dto);
        }
    }

    @PostMapping("/delete")
    public ResponseEntity<ProjectAssignmentDTO> deleteProjectAssignment(@RequestBody ProjectAssignmentDTO dto) {
        try
        {
            if (dto.getId() == null) {
                throw new Exception("ID NULL!");
            }
            projectAssignmentService.delete(dto.getId());
            return ResponseEntity.ok().body(dto);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.badRequest().header("X-Message", e.getMessage()).body(dto);
        }
    }
}
