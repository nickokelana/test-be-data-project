package id.co.cimbniaga.octomobile.project.service;

import id.co.cimbniaga.octomobile.project.component.AbstractEntityService;
import id.co.cimbniaga.octomobile.project.domain.dao.Project;
import id.co.cimbniaga.octomobile.project.domain.dto.common.ProjectDTO;
import id.co.cimbniaga.octomobile.project.domain.mapper.ProjectMapperImpl;
import id.co.cimbniaga.octomobile.project.repository.ProjectRepository;
import id.co.cimbniaga.octomobile.project.util.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class ProjectService extends AbstractEntityService<Project, ProjectDTO, Long> {

    @Autowired
    ValidationUtils validationUtils;

    public ProjectService(ProjectRepository ProjectRepository) {
        this.entityRepository = ProjectRepository;
        this.entityMapper = new ProjectMapperImpl();
    }

    public ProjectRepository repository() {
        return (ProjectRepository) entityRepository;
    }

    public void initData() throws ParseException {
        entityRepository.save(new Project("PROJECT001", "PROJECT SATU", "DESC",
                new SimpleDateFormat("dd/MM/yyyy").parse("01/02/2022"), new SimpleDateFormat("dd/MM/yyyy").parse("28/02/2022")));
    }

    public ProjectDTO create(ProjectDTO projectDTO) throws Exception {
        // validation
        try {
            validation(projectDTO);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        List<Project> exist = repository().findByCode(projectDTO.getCode());
        if (exist.size() > 0) {
            throw new Exception("Project code must be unique!");
        }

        ProjectDTO result = save(projectDTO);

        return result;
    }

    public ProjectDTO update(ProjectDTO projectDTO) throws Exception {
        // validation
        try {
            validation(projectDTO);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        List<Project> exist = repository().findByCode(projectDTO.getCode());
        if(exist.stream().filter(x -> x.getIdproject() != projectDTO.getId() && x.getCode().contains(projectDTO.getCode())).findAny().orElse(null) != null) {
            throw new Exception("Project code must be unique!");
        }

        ProjectDTO result = save(projectDTO);

        return result;
    }

    public Boolean validationDate(ProjectDTO projectDTO) {
        if (projectDTO.getDateStart().after(projectDTO.getDateEnd())) {
            return false;
        }
        return true;
    }

    @Override
    public void validation(ProjectDTO dto) throws Exception {
        if (validationUtils.isNullOrEmpty(dto.getCode())) {
            throw new Exception("field code is null or empty");
        }
        if (validationUtils.isNullOrEmpty(dto.getName())) {
            throw new Exception("field code is null or empty");
        }
        if (validationUtils.isNull(dto.getDateStart())) {
            throw new Exception("field dateStart is null");
        }
        if (validationUtils.isNull(dto.getDateEnd())) {
            throw new Exception("field dateStart is null");
        }
    }
}
