package id.co.cimbniaga.octomobile.project.domain.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Project {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long idproject;

    private String code;

    private String name;

    private String description;

    private Date datestart;

    private Date dateend;

    public Project() {
    }

    public Project(String code, String name, String description, Date datestart, Date dateend) {
        this.code = code;
        this.name = name;
        this.description = description;
        this.datestart = datestart;
        this.dateend = dateend;
    }

    public Long getIdproject() {
        return idproject;
    }

    public void setIdproject(Long idproject) {
        this.idproject = idproject;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDatestart() {
        return datestart;
    }

    public void setDatestart(Date datestart) {
        this.datestart = datestart;
    }

    public Date getDateend() {
        return dateend;
    }

    public void setDateend(Date dateend) {
        this.dateend = dateend;
    }

    @Override
    public String toString() {
        return "Project{" +
                "idproject=" + idproject +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", datestart=" + datestart +
                ", dateend=" + dateend +
                '}';
    }
}
