package id.co.cimbniaga.octomobile.project.repository;
import id.co.cimbniaga.octomobile.project.domain.dao.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query("select e from Employee e")
    Page<Employee> findAll(Pageable pageable);

    @Query("select e from Employee e where e.employeenumber=:employeeNumber")
    List<Employee> findByEmployeeNumber(@Param("employeeNumber") String employeeNumber);
}
