package id.co.cimbniaga.octomobile.project.service;

import id.co.cimbniaga.octomobile.project.component.AbstractEntityService;
import id.co.cimbniaga.octomobile.project.domain.dao.Employee;
import id.co.cimbniaga.octomobile.project.domain.dao.Project;
import id.co.cimbniaga.octomobile.project.domain.dao.ProjectAssignment;
import id.co.cimbniaga.octomobile.project.domain.dto.common.EmployeeDTO;
import id.co.cimbniaga.octomobile.project.domain.dto.common.ProjectAssignmentDTO;
import id.co.cimbniaga.octomobile.project.domain.dto.common.ProjectDTO;
import id.co.cimbniaga.octomobile.project.domain.dto.common.RequestAssignProjectDTO;
import id.co.cimbniaga.octomobile.project.domain.mapper.ProjectAssignmentMapperImpl;
import id.co.cimbniaga.octomobile.project.domain.mapper.ProjectMapperImpl;
import id.co.cimbniaga.octomobile.project.repository.ProjectAssignmentRepository;
import id.co.cimbniaga.octomobile.project.repository.ProjectRepository;
import id.co.cimbniaga.octomobile.project.util.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ProjectAssignmentService extends AbstractEntityService<ProjectAssignment, ProjectAssignmentDTO, Long> {

    @Autowired
    ValidationUtils validationUtils;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    ProjectService projectService;

    public ProjectAssignmentService(ProjectAssignmentRepository projectAssignmentRepository) {
        this.entityRepository = projectAssignmentRepository;
        this.entityMapper = new ProjectAssignmentMapperImpl();
    }

    public ProjectAssignmentRepository repository() {
        return (ProjectAssignmentRepository) entityRepository;
    }

    public void initData() throws ParseException {
        employeeService.initData();
        projectService.initData();
        List<EmployeeDTO> employees = employeeService.findAll(Pageable.unpaged()).getContent();
        List<ProjectDTO> projectDTOS = projectService.findAll(Pageable.unpaged()).getContent();
        ProjectAssignmentDTO projectAssignment = new ProjectAssignmentDTO();
        projectAssignment.setLeader(employees.get(0));
        projectAssignment.setProjectDTO(projectDTOS.get(0));
        projectAssignment.setDateCreated(new Date());
        projectAssignment.setDateModified(new Date());
        List<EmployeeDTO> employeeDTOS = new ArrayList<>();
        employeeDTOS.add(employees.get(1));
        employeeDTOS.add(employees.get(2));
        projectAssignment.setMembers(employeeDTOS);
        System.out.println(projectAssignment);
        save(projectAssignment);
    }

    public void assignMember(RequestAssignProjectDTO requestAssignProjectDTO) throws Exception {
        Project project = projectService.find(requestAssignProjectDTO.getProjectId());
        if (project == null) {
            throw new Exception("Project not found!");
        }
        Employee employee = employeeService.find(requestAssignProjectDTO.getEmployeeId());
        if (employee == null) {
            throw new Exception("Employee not found!");
        }

        ProjectAssignment projectAssignment = null;
        List<ProjectAssignment> exist = repository().findByProjectId(project.getIdproject());
        if (exist.size() > 0) {
            projectAssignment = exist.get(0);

            // CHECK IF LEADER
            Employee leader = projectAssignment.getLeader();
            if (leader != null) {
                if (leader.getIdemployee() == employee.getIdemployee()) {
                    throw new Exception("employee " + employee.getName() + " can't be assign to member because its already assign to leader on this project");
                }
            }

            // CHECK MEMBER EXIST
            if (projectAssignment.getMembers().stream().filter(x -> x.getIdemployee() == employee.getIdemployee()).findAny().orElse(null) != null) {
                throw new Exception("employee already assign");
            }

            projectAssignment.getMembers().add(employee);
        } else {
            projectAssignment = new ProjectAssignment();
            projectAssignment.setProject(project);
            projectAssignment.setDtCreated(new Date());
            projectAssignment.setDtModified(new Date());
            Set<Employee> employees = new HashSet<>();
            employees.add(employee);
            projectAssignment.setMembers(employees);
        }
        entityRepository.save(projectAssignment);
    }

    public void assignLeader(RequestAssignProjectDTO requestAssignProjectDTO) throws Exception {
        Project project = projectService.find(requestAssignProjectDTO.getProjectId());
        if (project == null) {
            throw new Exception("Project not found!");
        }
        Employee employee = employeeService.find(requestAssignProjectDTO.getEmployeeId());
        if (employee == null) {
            throw new Exception("Employee not found!");
        }

        ProjectAssignment projectAssignment = null;
        List<ProjectAssignment> exist = repository().findByProjectId(project.getIdproject());
        if (exist.size() > 0) {
            projectAssignment = exist.get(0);

            // IF ALREADY ASSIGN TO MEMBER, REMOVE IT
            if (projectAssignment.getMembers().stream().filter(x -> x.getIdemployee() == employee.getIdemployee()).findAny().orElse(null) != null) {
                projectAssignment.getMembers().remove(employee);
            }

            projectAssignment.setLeader(employee);
        } else {
            projectAssignment = new ProjectAssignment();
            projectAssignment.setProject(project);
            projectAssignment.setLeader(employee);
            projectAssignment.setDtCreated(new Date());
            projectAssignment.setDtModified(new Date());
        }
        entityRepository.save(projectAssignment);
    }

    public ProjectAssignmentDTO create(ProjectAssignmentDTO projectAssignmentDTO) throws Exception {
        // validation
        try {
            validation(projectAssignmentDTO);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        // merge data duplicate
        mergeDuplicate(projectAssignmentDTO);

        // check assignment for project
        List<ProjectAssignment> exist = repository().findByProjectId(projectAssignmentDTO.getProjectDTO().getProjectId());
        if (exist.size() > 0) {
            throw new Exception("Assignment for this project is already exist");
        }

        projectAssignmentDTO.setDateCreated(new Date());
        projectAssignmentDTO.setDateModified(new Date());
        ProjectAssignmentDTO result = save(projectAssignmentDTO);

        return result;
    }

    public ProjectAssignmentDTO update(ProjectAssignmentDTO projectAssignmentDTO) throws Exception {
        // validation
        try {
            validation(projectAssignmentDTO);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        // merge data duplicate
        mergeDuplicate(projectAssignmentDTO);

        // cant change the project
        Optional<ProjectAssignment> projectAssignment = repository().findById(projectAssignmentDTO.getId());
        if (projectAssignment.isEmpty()) {
            throw new Exception("Data not found!");
        }

        if (projectAssignment.get().getProject().getIdproject() != projectAssignmentDTO.getProjectDTO().getProjectId()) {
            throw new Exception("you can't change the project");
        }

        projectAssignmentDTO.setDateModified(new Date());
        ProjectAssignmentDTO result = save(projectAssignmentDTO);

        return result;
    }

    public Boolean validationDate(ProjectDTO projectDTO) {
        if (projectDTO.getDateStart().after(projectDTO.getDateEnd())) {
            return false;
        }
        return true;
    }

    @Override
    public void validation(ProjectAssignmentDTO dto) throws Exception {
        if (dto.getProjectDTO() == null) {
            throw new Exception("field project is null or empty");
        }
        if (dto.getLeader() == null) {
            throw new Exception("field leader is null");
        }
        if (dto.getMembers() == null) {
            throw new Exception("field members minimum 1");
        }
        if (dto.getMembers().size() < 1) {
            throw new Exception("field members minimum 1");
        }
        // 1 EMPLOYEE CAN'T BE ASSIGN to LEADER AND MEMBER AT THE SAME PROJECT
        EmployeeDTO employeeDTO = dto.getMembers().stream().filter(x -> x.getId() == dto.getLeader().getId()).findAny().orElse(null);
        if (employeeDTO != null) {
            throw new Exception("employee " + employeeDTO.getName() + " can't be assign to member because its already assign to leader on this project");
        }
    }

    public ProjectAssignmentDTO mergeDuplicate(ProjectAssignmentDTO projectAssignmentDTO) {
        List<EmployeeDTO> fixedData = new ArrayList<>();
        for(EmployeeDTO employeeDTO: projectAssignmentDTO.getMembers()) {
            if (fixedData.stream().filter(x -> x.getId() == employeeDTO.getId()).findAny().orElse(null) == null) {
                fixedData.add(employeeDTO);
            }
        }
        projectAssignmentDTO.setMembers(fixedData);
        return projectAssignmentDTO;
    }
}
