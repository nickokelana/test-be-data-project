package id.co.cimbniaga.octomobile.project.domain.mapper;

import id.co.cimbniaga.octomobile.project.component.EntityMapper;
import id.co.cimbniaga.octomobile.project.domain.dao.Employee;
import id.co.cimbniaga.octomobile.project.domain.dto.common.EmployeeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper()
public interface EmployeeMapper extends EntityMapper<EmployeeDTO, Employee> {

    @Mapping(source = "idemployee", target= "id")
    @Mapping(source = "idemployee", target= "employeeId")
    @Mapping(source = "employeenumber", target= "employeeNumber")
    @Mapping(source = "name", target= "name")
    @Mapping(source = "personalidnumber", target= "personalIdNumber")
    @Mapping(source = "active", target= "active")
    EmployeeDTO toDto(Employee employee);

    @Mapping(source = "employeeId", target= "idemployee")
    @Mapping(source = "employeeNumber", target= "employeenumber")
    @Mapping(source = "name", target= "name")
    @Mapping(source = "personalIdNumber", target= "personalidnumber")
    @Mapping(source = "active", target= "active")
    Employee toEntity(EmployeeDTO dto);

    default Employee fromId(Long id) {
        if (id == null) {
            return null;
        }
        Employee entity = new Employee();
        entity.setIdemployee(id);
        return entity;
    }
}
