package id.co.cimbniaga.octomobile.project.util;

import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ValidationUtils {
    public boolean isNullOrEmpty(String value) {
        if (value == null || value == "") {
            return true;
        }
        return false;
    }

    public boolean isNull(Date value) {
        if (value == null) {
            return true;
        }
        return false;
    }
}
