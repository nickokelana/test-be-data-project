package id.co.cimbniaga.octomobile.project.domain.dto.common;

import id.co.cimbniaga.octomobile.project.domain.dao.Employee;

import java.util.*;

public class ProjectAssignmentDTO {

    private Long id;

    private Long projectAssignmentId;

    private ProjectDTO projectDTO;

    private EmployeeDTO leader;

    private Date dateCreated;

    private Date dateModified;

    private List<EmployeeDTO> members = new ArrayList<>();

    public ProjectAssignmentDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProjectAssignmentId() {
        return projectAssignmentId;
    }

    public void setProjectAssignmentId(Long projectAssignmentId) {
        this.projectAssignmentId = projectAssignmentId;
    }

    public ProjectDTO getProjectDTO() {
        return projectDTO;
    }

    public void setProjectDTO(ProjectDTO projectDTO) {
        this.projectDTO = projectDTO;
    }

    public EmployeeDTO getLeader() {
        return leader;
    }

    public void setLeader(EmployeeDTO leader) {
        this.leader = leader;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public List<EmployeeDTO> getMembers() {
        return members;
    }

    public void setMembers(List<EmployeeDTO> members) {
        this.members = members;
    }
}
