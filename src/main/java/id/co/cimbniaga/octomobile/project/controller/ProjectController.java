package id.co.cimbniaga.octomobile.project.controller;

import id.co.cimbniaga.octomobile.project.domain.dto.common.ProjectDTO;
import id.co.cimbniaga.octomobile.project.service.ProjectService;
import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/v1/project")
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @PostMapping("/init")
    public ResponseEntity<String> initData() {
        try {
            projectService.initData();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok("OK");
    }

    @PostMapping("")
    public ResponseEntity<List<ProjectDTO>> getProject(Pageable pageable) {
        Page<ProjectDTO> page = projectService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PostMapping("/detail/{id}")
    public ResponseEntity<ProjectDTO> getProjectById(@PathVariable Long id) {
        ProjectDTO result = projectService.findOne(id);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/create")
    public ResponseEntity<ProjectDTO> createProject(@RequestBody ProjectDTO dto) {
        try
        {
            if (dto.getId() != null) {
                throw new Exception("ID NOT NULL!");
            }
            ProjectDTO data = projectService.create(dto);
            return ResponseEntity.ok().body(data);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.badRequest().header("X-Message", e.getMessage()).body(dto);
        }
    }

    @PostMapping("/update")
    public ResponseEntity<ProjectDTO> updateProject(@RequestBody ProjectDTO dto) {
        try
        {
            if (dto.getId() == null) {
                throw new Exception("ID NULL!");
            }
            ProjectDTO data = projectService.update(dto);
            return ResponseEntity.ok().body(data);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.badRequest().header("X-Message", e.getMessage()).body(dto);
        }
    }

    @PostMapping("/delete")
    public ResponseEntity<ProjectDTO> deleteProject(@RequestBody ProjectDTO dto) {
        try
        {
            if (dto.getId() == null) {
                throw new Exception("ID NULL!");
            }
            projectService.delete(dto.getId());
            return ResponseEntity.ok().body(dto);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.badRequest().header("X-Message", e.getMessage()).body(dto);
        }
    }
}
